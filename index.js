const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = +process?.env?.PORT || 56201;

app.use(bodyParser.text());

app.post('/square', (req, res) => {

    if (isNaN(+req.body)) return res.status(400).json({ "error": "value is not a number" });
    
    let number = +req.body;
    res.set('Content-Type', 'application/json');
    return res.json({ "number": number, "square": number**2 });
});

app.post('/reverse', (req, res) => {

    let str = String(req.body);
    if (str.length == 0) return res.json({ "reversed": "" });

    return res.send(str.split("").reverse().join(""));
});

const isLeapYear = (year) => {

    if (year % 4) return false;
    if (!(year % 100) && year % 400) return false;

    return true;
};

app.get('/date/:year/:month/:day', (req, res) => {
    
    if (isNaN(+req.params.year)) return res.status(400).json({ "error": `${req.params.year} is not a number` });
    if (isNaN(+req.params.month)) return res.status(400).json({ "error": `${req.params.month} is not a number` });
    if (isNaN(+req.params.day)) return res.status(400).json({ "error": `${req.params.day} is not a number` });

    let year = +req.params.year;
    let month = +req.params.month;
    let day = +req.params.day;

    let daysInMonth = [31, 28, 41, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    let daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const isYearLeapYear = isLeapYear(year);
    if (isYearLeapYear) daysInMonth[1] += 1;

    if (month < 1 || month > 12) return res.status(400).json({ "error": `invalid month value` });
    if (day < 1 || day > daysInMonth[month - 1]) return res.status(400).json({ "error": `invalid day value` });

    let currentDate = new Date();
    let providedDate = new Date(year,  month - 1, day);
    
    let msDiff = providedDate - currentDate;
    let msInDay = 1000 * 60 * 60 * 24;

    let difference = Math.floor( Math.abs(msDiff) / msInDay );
    if (msDiff >= 0) difference += 1;

    return res.json({
        "weekDay": daysOfWeek[providedDate.getDay()],
        "isLeapYear": isYearLeapYear,
        "difference": difference
    })
});

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});